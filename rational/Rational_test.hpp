//
//  Rational_test.hpp
//  Rational
//
//  Created by Kevin H. Patterson on 2/10/19.
//  Copyright © 2019 Creltek. All rights reserved.
//

#ifndef Rational_test_hpp
#define Rational_test_hpp

#include "Rational.hpp"
#include <iostream>
#include <cmath>

using namespace std;

template<typename Word_T>
class Rational_test {
public:
    using int_n = typename make_signed<Word_T>::type;
    using uint_n = typename make_unsigned<Word_T>::type;

    bool test() {
        bool Pass = true;
        
        {
            cout << "Testing sqrt_est( int )..." << endl;
            double max_er = 0;
            double avg_er = 0;
            int i = 1;
            for( ; i <= 100000000; ++i ) {
                Rational<int> r = sqrt_est( i );
                double rr = static_cast<double>(r);
                double cr = sqrt( i );
                double er = abs( (rr-cr) / cr );
                if( er > max_er ) max_er = er;
                avg_er = avg_er + er;
//                cout << "√" << i << " = " << cr << " ≅ " << r << " ≅ " << rr << ", error = " << 100.0 * er << "%" << endl;
            }
            cout << "Max Error: " << 100.0 * max_er << "%, Average Error: " << 100.0 * avg_er / double(i-1) << "%" << endl << endl;
        }
        
        {
            cout << "Testing sqrt_est( Rational )..." << endl;
            double max_er = 0;
            double avg_er = 0;
            int i = 1;
            for( int n = 1; n <= 16384; ++n ) {
                for( int d = 1; d <= 16384; ++d ) {
                    Rational<int> r = sqrt_est( Rational<int>( n, d ) );
                    double rr = static_cast<double>(r);
                    double cr = sqrt( double(n) / double(d) );
                    double er = abs( (rr-cr) / cr );
                    if( er > max_er ) max_er = er;
                    avg_er = avg_er + er;
//                    cout << "√" << i << " = " << cr << " ≅ " << r << " ≅ " << rr << ", error = " << 100.0 * er << "%" << endl;
                    ++i;
                }
            }
            cout << "Max Error: " << 100.0 * max_er << "%, Average Error: " << 100.0 * avg_er / double(i-1) << "%" << endl << endl;
        }

        const int_n smax = (static_cast<uint_n>(-1) >> 1);
        const int_n smin = -smax;

//        const uint_n umax = smax; // static_cast<uint_n>(-1);
//        const uint_n umin = 0;

        cout << "word_smin: " << +smin << endl;
        cout << "word_smax: " << +smax << endl;
//        cout << "word_umin: " << +umin << endl;
//        cout << "word_umax: " << +umax << endl;
        cout << endl;

        // check integer storage
        {
            int_n n = smin;
            do {
                Rational<Word_T> r{ n };
                r.normalize();
                r.reduce_2();
                const int_n xn = r.quotient();
                if( xn != n ) {
                    cout << "** storage error: " << +n << "/" << +1 << " => " << +xn << "/" << +1 << endl;
                    Pass = false;
                }
            } while( n++ < smax );
        }

        // check denominator storage
        {
            int_n d = smin;
            do {
                if( d == 0 )
                    continue;
                
                Rational<Word_T> r{ 1, d };
                r.normalize();
                r.reduce_2();
                const int_n xd = r.dnm;
                if( d < 0 ) {
                    if( -xd != d ) {
                        cout << "** storage error: " << +1 << "/" << +d << " => " << r.num << "/" << +xd << endl;
                        Pass = false;
                    }
                } else {
                    if( xd != d ) {
                        cout << "** storage error: " << +1 << "/" << +d << " => " << r.num << "/" << +xd << endl;
                        Pass = false;
                    }
                }

            } while( d++ < smax );
        }

        // check storage and equivalency for all fractions
        {
            int_n d = smin;
            do {
                if( d == 0 )
                    continue;
                
                int_n n = smin;
                do {
                    Rational<Word_T> r{ n, d };
                    const int_n xn = r.num;
                    const int_n xd = r.dnm;
                    if( xn != n || xd != d ) {
                        cout << "** storage error: " << +n << "/" << +d << " => " << +xn << "/" << +xd << endl;
                        Pass = false;
                    }
                    
                    if( d != 0 ) {
                        if( !test_equivalency( n, d, r.num, r.dnm ) )
                            Pass = false;
                        {
                            Rational<Word_T> r2 = r;
                            r2.normalize();
                            if( !test_equivalency( n, d, r2.num, r2.dnm ) )
                                Pass = false;
                        }
                        {
                            Rational<Word_T> r2 = r;
                            r2.reduce_2();
                            if( !test_equivalency( n, d, r2.num, r2.dnm ) )
                                Pass = false;
                        }
                        {
                            Rational<Word_T> r2 = r;
                            r2.reduce();
                            if( !test_equivalency( n, d, r2.num, r2.dnm ) )
                                Pass = false;
                        }
                        {
                            Rational<Word_T> r2 = r;
                            r2.simplify();
                            if( !test_equivalency( n, d, r2.num, r2.dnm ) )
                                Pass = false;
                        }
                    }
                } while( n++ < smax );
            } while( d++ < smax );
        }
        
        const int_n nmin = -127;
        const int_n nmax = 127;

        const int_n dmin = -127;
        const int_n dmax = 127;

//        const uint_n dmin = 0;
//        const uint_n dmax = 127;

        double epsilon_add = 1.0/256;
        double epsilon_sub = 1.0/256;
        double epsilon_mul = 1.0/256;
        double epsilon_div = 1.0/256;
        // check arithmetic operators for all fractions
        {
            int_n d = dmin;
            do {
                
                if( d == 0 )
                    continue;
                
                int_n n = nmin;
                do {
                    Rational<Word_T> r( n, d );
                    cout << r << endl;
                    int_n d2 = dmin;
                    do {
                        if( d2 == 0 )
                            continue;
                        
                        int_n n2 = nmin;
                        do {
                            Rational<Word_T> r2( n2, d2 );
                            
                            { // add
//                                if( r.num == 1 && r.dnm == 127 && r2.num == 1 && r2.dnm == 126 ) {
//                                    cout << "break" << endl;
//
                                double a = (n/double(d)) + (n2/double(d2));
                                if( a >= smin && a <= smax ) {
                                    try {
                                        Rational<Word_T> a2 = r + r2;
                                        double error = fabs( a - (double)a2 );
                                        if( error > epsilon_add ) {
                                            cout << "** arithmetic error: " << r << "+" << r2 << "=" << a2 << ": " << error << endl;
                                            epsilon_add = error;
                                        }
                                    } catch(...) {
//                                        cout << "** (exception)" << endl;
                                    }
                                }
//                                }
                            }
                            
                            { // sub
                                double a = (n/double(d)) - (n2/double(d2));
                                if( a >= smin && a <= smax ) {
                                    try {
                                        Rational<Word_T> a2 = r - r2;
                                        double error = fabs( a - (double)a2 );
                                        if( error > epsilon_sub ) {
                                            cout << "** arithmetic error: " << r << "-" << r2 << "=" << a2 << ": " << error << endl;
                                            epsilon_sub = error;
                                        }
                                    } catch(...) {
//                                        cout << "** (exception)" << endl;
                                    }
                                }
                            }

                            { // mul
                                double a = double(n*n2) / double(d*d2);
                                if( a >= smin && a <= smax ) {
                                    try {
                                        Rational<Word_T> a2 = r * r2;
                                        double error = fabs( a - (double)a2 );
                                        if( error > epsilon_mul ) {
                                            cout << "** arithmetic error: " << r << "*" << r2 << "=" << a2 << ": " << error << endl;
                                            epsilon_mul = error;
                                        }
                                    } catch(...) {
//                                        cout << "** (exception)" << endl;
                                    }
                                }
                            }

                            { // div
                                double a = double(n*d2) / double(d*n2);
                                if( a >= smin && a <= smax ) {
                                    try {
                                        Rational<Word_T> a2 = r / r2;
                                        double error = fabs( a - (double)a2 );
                                        if( error > epsilon_div ) {
                                            cout << "** arithmetic error: " << r << "/" << r2 << "=" << a2 << ": " << error << endl;
                                            epsilon_div = error;
                                        }
                                    } catch(...) {
//                                        cout << "** (exception)" << endl;
                                    }
                                }
                            }
                            
                        } while( n2++ < nmax );
                    } while( d2++ < dmax );
                } while( n++ < nmax );
            } while( d++ < dmax );
        }
        
        return Pass;
    }
    
    // gcd = bits * 2^x
    struct gcd {
        uint_n bits;
        unsigned int x;
        
        uint_n n() const { return bits << x; }
    };
    
    static bool is_even( uint_n n ) {
        return (n & 1) == 0;
    }
    
    // implementation of Stein's algorithm
    static gcd find_gcd( int_n i_a, uint_n b ) {
        if( i_a == 0 || b == 0 )
            return gcd{ 1, 0 };
        
        uint_n a;
        if( i_a < 0 )
            a = -i_a;
        else
            a = i_a;
        
        if( a == 0 )
            return gcd{ b, 0 };
        
        if( b == 0 )
            return gcd{ a, 0 };
        
        uint_n x = 0;
        while( is_even(a) && is_even(b) ) {
            a >>= 1;
            b >>= 1;
            ++x;
        }
        while( a != b ) {
            if( is_even(a) )
                a >>= 1;
            else if( is_even(b) )
                b >>= 1;
            else if( a > b )
                a = (a - b) >> 1;
            else
                b = (b - a) >> 1;
        }
        return gcd{ a, x };
    }
    
    static bool test_equivalency( int_n n, int_n d, int_n xn, int_n xd, bool Display = true ) {
        if( n == 0 && xn == 0 )
            return true;

        bool Pass = true;
        
        if( d < 0 ) {
            n = -n;
            d = -d;
        }
        gcd g = find_gcd( n, d );
        int_n rn = n / g.n();
        int_n rd = d / g.n();
        
        if( xd < 0 ) {
            xn = -xn;
            xd = -xd;
        }
        gcd xg = find_gcd( xn, xd );
        int_n xrn = xn / xg.n();
        int_n xrd = xd / xg.n();
        
        if( xrn != rn || xrd != rd ) {
            if( Display )
                cout << "** equivalency error: " << +rn << "/" << +rd << " != " << +xrn << "/" << +xrd << endl;
            
            Pass = false;
        }
        
        return Pass;
    }
};
    
#endif /* Rational_test_hpp */
