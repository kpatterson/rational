//
// Complex.hpp
// Rational
//
// Created by Kevin H. Patterson on 2/10/19.
// Copyright © 2019 Creltek. All rights reserved.
//

#ifndef Complex_hpp
#define Complex_hpp

#define COMPLEX_OSTREAM_INSERTER

#include <cmath>
#ifdef COMPLEX_OSTREAM_INSERTER
    #include <iostream>
#endif

template<typename T>
class Complex {
public:
    T real;
    T imag;
    
public:
    Complex()
    : real{ 0 }
    , imag{ 0 }
    {};
    
    Complex( const T& i_real )
    : real{ i_real }
    , imag{ 0 }
    {};

    Complex( const T& i_real, const T& i_imag )
    : real{ i_real }
    , imag{ i_imag }
    {};

    Complex( const float i_real )
    : real{ i_real }
    , imag{ 0 }
    {};
    
    Complex( const double i_real )
    : real{ i_real }
    , imag{ 0 }
    {};

    Complex( const float i_real, const float i_imag )
    : real{ i_real }
    , imag{ i_imag }
    {};
    
    Complex( const double i_real, const double i_imag )
    : real{ i_real }
    , imag{ i_imag }
    {};
    
    Complex<T>( const Complex<T>& c )
    : real{ c.real }
    , imag{ c.imag }
    {};
    
    Complex<T>& operator +=( const Complex<T>& rhs );
    Complex<T>& operator -=( const Complex<T>& rhs );
    Complex<T>& operator *=( const Complex<T>& rhs );
    Complex<T>& operator /=( const Complex<T>& rhs );
    Complex<T> operator +( const Complex<T>& rhs ) const;
    Complex<T> operator -( const Complex<T>& rhs ) const;
    Complex<T> operator *( const Complex<T>& rhs ) const;
    Complex<T> operator /( const Complex<T>& rhs ) const;
    Complex<T> operator -() const;
    
    T norm() const {
        return real * real + imag * imag;
    }
    
    T abs() const {
        return sqrt( norm() );
    }
    
    T arg() const {
        return atan2( imag, real );
    }
    
    Complex<T> conj() const {
        return Complex<T>{ real, -imag };
    }
    
    static Complex<T> polar( const T& rho, const T& theta ) {
        return Complex<T>{ rho * cos( theta ), rho * sin( theta ) };
    }

#ifdef COMPLEX_OSTREAM_INSERTER
    template<typename U>
    friend std::ostream& operator <<( std::ostream& os, const Complex<U>& c );
#endif
};

template<typename T>
Complex<T>& Complex<T>::operator +=( const Complex<T>& rhs ) {
    real += rhs.real;
    imag += rhs.imag;
    return *this;
}

template<typename T>
Complex<T>& Complex<T>::operator -=( const Complex<T>& rhs ) {
    real -= rhs.real;
    imag -= rhs.imag;
    return *this;
}

template<typename T>
Complex<T>& Complex<T>::operator *=( const Complex<T>& rhs ) {
    T t_real = real * rhs.real - imag * rhs.imag;
    T t_imag = real * rhs.imag + imag * rhs.real;
    real = t_real;
    imag = t_imag;
    return *this;
}

template<typename T>
Complex<T>& Complex<T>::operator /=( const Complex<T>& rhs ) {
    Complex<T> nm = Complex<T>{ *this } * Complex<T>{ rhs.real, -rhs.imag };
    T dnm = rhs.real * rhs.real + rhs.imag * rhs.imag;
    real = nm.real / dnm;
    imag = nm.imag / dnm;
    return *this;
}

template<typename T>
Complex<T> Complex<T>::operator +( const Complex<T>& rhs ) const {
    return Complex<T>{ *this } += rhs;
}

template<typename T>
Complex<T> Complex<T>::operator -( const Complex<T>& rhs ) const {
    return Complex<T>{ *this } -= rhs;
}

template<typename T>
Complex<T> Complex<T>::operator *( const Complex<T>& rhs ) const {
    return Complex<T>{ *this } *= rhs;
}

template<typename T>
Complex<T> Complex<T>::operator /( const Complex<T>& rhs ) const {
    return Complex<T>{ *this } /= rhs;
}

template<typename T>
Complex<T> Complex<T>::operator -() const {
    return Complex<T>{ -real, -imag };
}

#ifdef COMPLEX_OSTREAM_INSERTER
template<typename T>
std::ostream& operator <<( std::ostream& os, const Complex<T>& c ) {
    os << "(" << c.real;
    if( c.imag < 0 )
        os << c.imag << "i" << ")";
    else if( c.imag > 0 )
        os << "+" << c.imag << "i)";
    return os;
}
#endif

#endif /* Complex_hpp */
