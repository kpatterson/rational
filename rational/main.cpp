//
//  main.cpp
//  Rational
//
//  Created by Kevin H. Patterson on 1/23/19.
//  Copyright © 2019 Creltek. All rights reserved.
//

#include <iostream>
#include "Rational.hpp"
#include "Rational_math.hpp"
#include "Rational_test.hpp"
#include "Complex.hpp"

using namespace std;
/*
 -9 +  -10
  5      9

 -9*9 + -10*5
  5*9     9*5

-81 +  -50
 45     45

254  =  127
3        63
*/

typedef Rational<int> R;

struct RPoint {
    R x;
    R y;
};

struct LineStd;

struct LineSI {
    R m;
    R b;
    
    LineStd to_Std() const;
};

struct LineStd {
    R A;
    R B;
    R C;
    
    LineSI to_SI() const {
        A.reduce();
        B.reduce();
        C.reduce();
        
        LineSI L;
        
        L.m = -A/B;
        L.b = C/B;
        L.m.reduce();
        L.b.reduce();
        
        return L;
    }
};

LineStd LineSI::to_Std() const {
    m.reduce();
    b.reduce();
    
    LineStd L;
    L.A = -m;
    L.B = 1;
    L.C = b;
    
    int gcd = find_gcd_n( L.A.dnm, L.C.dnm );
    int lcm = L.A.dnm * L.C.dnm / gcd;
    if( L.A.sign() < 0 )
        lcm = -lcm;
    
    L.A *= lcm;
    L.B *= lcm;
    L.C *= lcm;
    
    L.A.reduce();
    L.C.reduce();
    
    return L;
}

int GetInt() {
    string s;
    getline( cin, s );
    return std::stoi( s );
}

R GetRational() {
    string s;
    getline( cin, s );
    return R( s );
}

RPoint GetPoint() {
    cout << "x: ";
    R x = GetRational();
    cout << "y: ";
    R y = GetRational();
    return RPoint{ x, y };
}

void OutputLineEqn( const LineSI& LSI, const LineStd& LStd ) {
    cout << "y = " << LSI.m << "x " << (LSI.b.sign() >= 0 ? "+" : "") << LSI.b << endl;
    cout << LStd.A << "x " << (LStd.B.sign() >= 0 ? "+" : "") << LStd.B << "y = " << LStd.C << endl;
}

int main(int argc, const char * argv[]) {
    bool Running = true;
    bool Test = false;
    
    while( Running ) {
        LineSI LSI;
        LineStd LStd;
        
        int s = 0;
        while( s < 1 || s > 6 ) {
            cout << endl;
            cout << "Enter a line:" << endl;
            cout << "  1. In Slope-Intercept Form" << endl;
            cout << "  2. In Standard Form" << endl;
            cout << "  3. As a Point and Slope" << endl;
            cout << "  4. As two Points" << endl;
            cout << "  5. As a Point and y-intercept" << endl;
            cout << "  6. As a Point and x-intercept" << endl;
            cout << "  8. Run Test" << endl;
            cout << "  9. Quit" << endl;
            cout << endl << "Selection: ";
            s = GetInt();
        
            switch( s ) {
                case 9: {
                    Running = false;
                } break;
                case 8: {
                    Running = false;
                    Test = true;
                } break;
                case 1: {
                    cout << "m: ";
                    LSI.m = GetRational();
                    cout << "b: ";
                    LSI.b = GetRational();
                    
                    LStd = LSI.to_Std();
                } break;
                case 2: {
                    cout << "A (left side): ";
                    LStd.A = GetRational();
                    cout << "B (left side): ";
                    LStd.B = GetRational();
                    cout << "C (right side): ";
                    LStd.C = GetRational();
                    
                    LSI = LStd.to_SI();
                } break;
                case 3: {
                    cout << "m: ";
                    LSI.m = GetRational();
                    
                    cout << endl << "Point:" << endl;
                    RPoint P = GetPoint();
                    
                    // find a new b
                    // y = mx + b
                    LSI.b = P.y - LSI.m * P.x;
                    
                    LStd = LSI.to_Std();
                } break;
                case 4: {
                    cout << endl << "Point 1:" << endl;
                    RPoint P1 = GetPoint();
                    
                    cout << endl << "Point 2:" << endl;
                    RPoint P2 = GetPoint();
                    
                    LSI.m = (P2.y - P1.y) / (P2.x - P1.x);
                    LSI.b = P1.y - LSI.m * P1.x;
                    
                    LStd = LSI.to_Std();
                } break;
                case 5: {
                    cout << endl << "Point:" << endl;
                    RPoint P1 = GetPoint();
                    
                    cout << endl << "y-intercept: ";
                    RPoint P2{ 0, GetRational() };
                    
                    LSI.m = (P2.y - P1.y) / (P2.x - P1.x);
                    LSI.b = P1.y - LSI.m * P1.x;
                    
                    LStd = LSI.to_Std();
                } break;
                case 6: {
                    cout << endl << "Point:" << endl;
                    RPoint P1 = GetPoint();
                    
                    cout << endl << "x-intercept: ";
                    RPoint P2{ GetRational(), 0 };
                    
                    LSI.m = (P2.y - P1.y) / (P2.x - P1.x);
                    LSI.b = P1.y - LSI.m * P1.x;
                    
                    LStd = LSI.to_Std();
                } break;
                default: {
                    s = 0;
                } break;
            }
            if( s >= 8 ) {
                s = 1;
                continue;
            }
        }
        if( !Running )
            continue;
        
        cout << endl << "Your line:" << endl;
        OutputLineEqn( LSI, LStd );
        
        s = 0;
        while( s < 1 || s > 3 ) {
            cout << endl;
            cout << "Find parallel and perpendicular lines:" << endl;
            cout << "  1. Through a Point" << endl;
            cout << "  2. At y-intercept" << endl;
            cout << "  3. At x-intercept" << endl;
            cout << "  8. Skip" << endl;
            cout << "  9. Quit" << endl;
            cout << endl << "Selection: ";
            s = GetInt();

            RPoint P;
            
            switch( s ) {
                case 9: {
                    Running = false;
                } break;
                case 8: {
                } break;
                case 1: {
                    cout << endl << "Point:" << endl;
                    P = GetPoint();
                } break;
                case 2: {
                    cout << endl << "y-intercept: ";
                    P.x = 0;
                    P.y = GetRational();
                } break;
                case 3: {
                    cout << endl << "x-intercept: ";
                    P.x = GetRational();
                    P.y = 0;
                } break;
                default: {
                    s = 0;
                } break;
            }
            if( s >= 8 ) {
                s = 1;
                continue;
            }

            LSI.b = P.y - LSI.m * P.x;
            LStd = LSI.to_Std();
            
            cout << endl << "Parallel line:" << endl;
            OutputLineEqn( LSI, LStd );
            
            LSI.m.reciprocate();
            LSI.m = -LSI.m;
            LSI.b = P.y - LSI.m * P.x;
            LStd = LSI.to_Std();
            
            cout << endl << "Perpendicular line:" << endl;
            OutputLineEqn( LSI, LStd );
        }
    }

    if( !Test )
        return 0;
    
    cout << endl << "Testing Rational class..." << endl << endl;
    
    Rational_test<uint8_t> TestHarness;
    bool test_result = TestHarness.test();
    cout << endl;
    
    cout << "Test Result: " << (test_result ? "Pass" : "Fail") << endl;

    return 0;
}
