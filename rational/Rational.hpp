//
//  Rational.hpp
//  Rational
//
//  Created by Kevin H. Patterson on 1/23/19.
//  Copyright © 2019 Creltek. All rights reserved.
//

#ifndef Rational_hpp
#define Rational_hpp

#define EQU_DNM_TEST 1
#define INTERNAL_EQU_TEST 1
#define RATIONAL_OSTREAM_INSERTER 1
#define RATIONAL_HI_ACCURACY 1
#define RATIONAL_EXACT_THROW 1

#include "templ_math.hpp"

#include <cstdint>
#include <type_traits>
#include <limits>
#include <cassert>
#ifdef RATIONAL_OSTREAM_INSERTER
    #include <iostream>
#endif

template<typename Word_T = int>
class Rational {
public:
    using int_n = typename std::make_signed<Word_T>::type;
    using uint_n = typename std::make_unsigned<Word_T>::type;

    static constexpr int BITS_N = std::numeric_limits<uint_n>::digits;
    static constexpr int BITS_W = BITS_N * 2;

    using uint_w = typename sized_uint<BITS_W>::type;
    using int_w = typename std::make_signed<uint_w>::type;
    
    static constexpr uint_n HI_BIT_U = (static_cast<uint_n>(-1) >> 1) + 1;
    static constexpr int_n HI_BIT_S = (static_cast<uint_n>(-1) >> 1) + 1;

    int_n num;
    int_n dnm;
    
    struct flags_t {
        flags_t()
        : imprecise { false }
        , overflow { false }
        , underflow { false }
        , divzero { false }
        {}
        
        bool imprecise : 1;
        bool overflow : 1;
        bool underflow : 1;
        bool divzero : 1;
    } flags;
    
    Rational()
    : num{ 0 }
    , dnm{ 1 }
    {}
    
    Rational( const std::string& str )
    : num{ 0 }
    , dnm{ 1 }
    {
        size_t pos = str.find( "/" );
        if( pos == std::string::npos ) {
            num = std::stoi( str );
        } else {
            num = std::stoi( str.substr( 0, pos ) );
            dnm = std::stoi( str.substr( pos + 1 ) );
        }
    }

    Rational( int_n i_num )
    : num{ i_num }
    , dnm{ 1 }
    {
        assert( num != HI_BIT_S );
    }

    Rational( int_n i_num, int_n i_dnm )
    : num{ i_num }
    , dnm{ i_dnm }
    {
        assert( dnm != 0 );
        assert( num != HI_BIT_S );
        assert( dnm != HI_BIT_S );
    }
    
    Rational( float i_Float )
    {
        //TODO: Compute a continued fraction
        assert( dnm != 0 );
    }

    Rational( double i_Float )
    {
        //TODO: Compute a continued fraction
        assert( dnm != 0 );
    }

    Rational( const Rational& i_Rational )
    : Rational{ i_Rational.num, i_Rational.dnm }
    {}
    
    // true if numerator != 0
    operator bool() const { return num; }
    
    operator float() const { return float( num ) / dnm; }

    operator double() const { return double( num ) / dnm; }
    
    Rational& operator *=( const Rational& rhs ) {
        int_w n = rhs.num;
        n *= num;
        int_w d = rhs.dnm;
        d *= dnm;
        narrow( n, d, num, dnm );
        return *this;
    }
    
    Rational operator *( const Rational& rhs ) const {
        return Rational( *this ) *= rhs;
    }

    Rational& operator /=( const Rational& rhs ) {
        int_w n = rhs.dnm;
        int_w d = rhs.num;
        n *= num;
        d *= dnm;
        assert( dnm != 0 );
        narrow( n, d, num, dnm );
        return *this;
    }
    
    Rational operator /( const Rational& rhs ) const {
        return Rational( *this ) /= rhs;
    }
    
    Rational operator -() const {
        return Rational( -num, dnm );
    }

    Rational& operator +=( const Rational& rhs ) {
        int_w d = dnm;
        int_w n = num;
        if( dnm == rhs.dnm ) {
            n += rhs.num;
        } else {
            d *= rhs.dnm;
            n *= rhs.dnm;
            n += static_cast<int_w>( rhs.num ) * dnm;
        }
        narrow( n, d, num, dnm );
        return *this;
    }
    
    Rational operator +( const Rational& rhs ) const {
        return Rational( *this ) += rhs;
    }

    Rational& operator -=( const Rational& rhs ) {
        int_w d = dnm;
        int_w n = num;
        if( dnm == rhs.dnm ) {
            n -= rhs.num;
        } else {
            d *= rhs.dnm;
            n *= rhs.dnm;
            n -= static_cast<int_w>( rhs.num ) * dnm;
        }
        narrow( n, d, num, dnm );
        return *this;
    }
    
    Rational operator -( const Rational& rhs ) const {
        return Rational( *this ) -= rhs;
    }
    
    Rational& operator ++() {
        int_w d = dnm;
        int_w n = num;
        n += d;
        narrow( n, d, num, dnm );
        return *this;
    }

    Rational& operator --() {
        int_w d = dnm;
        int_w n = num;
        n -= d;
        narrow( n, d, num, dnm );
        return *this;
    }

    Rational operator ++(int) {
        Rational sav( *this );
        int_w d = dnm;
        int_w n = num;
        n += d;
        narrow( n, d, num, dnm );
        return sav;
    }
    
    Rational operator --(int) {
        Rational sav( *this );
        int_w d = dnm;
        int_w n = num;
        n -= d;
        narrow( n, d, num, dnm );
        return sav;
    }
    
    bool operator ==( const Rational& rhs ) const {
        if( num == 0 && rhs.num == 0 )
            return true;
#ifdef INTERNAL_EQU_TEST
        if( dnm == rhs.dnm && num == rhs.num )
            return true;
#endif
        normalize();
        rhs.normalize();
        return (num == rhs.num) && (dnm == rhs.dnm);
    }
    
    bool operator !=( const Rational& rhs ) const {
        return !(*this == rhs);
    }
    
    bool operator <( const Rational& rhs ) const {
#ifdef EQU_DNM_TEST
        if( dnm == rhs.dnm )
            return num < rhs.num;
#endif
        return static_cast<int_w>( num ) * rhs.dnm < static_cast<int_w>( rhs.num ) * dnm;
    }

    bool operator >( const Rational& rhs ) const {
#ifdef EQU_DNM_TEST
        if( dnm == rhs.dnm )
            return num > rhs.num;
#endif
        return static_cast<int_w>( num ) * rhs.dnm > static_cast<int_w>( rhs.num ) * dnm;
    }

    bool operator <=( const Rational& rhs ) const {
#ifdef EQU_DNM_TEST
        if( dnm == rhs.dnm )
            return num <= rhs.num;
#endif
        return static_cast<int_w>( num ) * rhs.dnm <= static_cast<int_w>( rhs.num ) * dnm;
    }
    
    bool operator >=( const Rational& rhs ) const {
#ifdef EQU_DNM_TEST
        if( dnm == rhs.dnm )
            return num >= rhs.num;
#endif
        return static_cast<int_w>( num ) * rhs.dnm >= static_cast<int_w>( rhs.num ) * dnm;
    }

    // normalize zero to 0/1
    inline bool normalize_zero() const {
        if( num == 0 ) {
            const_cast<int_n&>( dnm ) = 1;
            return true;
        }
        return false;
    }

    // normalize one to 1/1
    inline bool normalize_one() const {
        if( abs( num ) == dnm ) {
            if( num < 0 )
                const_cast<int_n&>( num ) = -1;
            else
                const_cast<int_n&>( num ) = 1;
            
            const_cast<int_n&>( dnm ) = 1;
            return true;
        }
        return false;
    }
    
    // normalize sign to numerator only
    inline bool normalize_sign() const {
        if( dnm < 0 ) {
            const_cast<int_n&>( num ) = -num;
            const_cast<int_n&>( dnm ) = -dnm;
            return true;
        }
        return false;
    }
    
    // normalize (maximum shift left)
    void normalize() const {
        assert( dnm != 0 );

        if( normalize_zero() )
            return;
        
        normalize_sign();
        
        if( normalize_one() )
            return;
        
        uint_n b = abs_tr<uint_n>( num );
        b |= dnm;
        const unsigned int lz = clz( b );
        if( lz > 1 ) {
            const_cast<int_n&>( num ) <<= lz - 1;
            const_cast<int_n&>( dnm ) <<= lz - 1;
        }
    }
    
    // reduce by powers of 2 (maximum shift right)
    void reduce_2() const {
        assert( dnm != 0 );
        
        if( normalize_zero() )
            return;
        
        normalize_sign();
        
        if( normalize_one() )
            return;
        
        uint_n b = abs_tr<uint_n>( num );
        b |= dnm;
        const unsigned int rz = ctz( b );
        if( rz ) {
            const_cast<int_n&>( num ) >>= rz;
            const_cast<int_n&>( dnm ) >>= rz;
        }
    }
    
    // reduce using greatest common divisor
    void reduce() const {
        assert( dnm != 0 );
//        if( dnm == 0 )
//            return;
        
        if( normalize_zero() )
            return;
        
        normalize_sign();
        
        if( normalize_one() )
            return;
        
        gcd<int_n> g = find_gcd( num, dnm );
        const_cast<int_n&>( num ) /= g.n();
        const_cast<int_n&>( dnm ) /= g.n();
    }

    // reduce by greatest common divisor, then normalize
    inline void simplify() const {
        reduce();
        normalize();
    }
    
    inline int_n sign() const {
        if( num == 0 )
            return 0;
        if( (num < 0) ^ (dnm < 0) )
            return -1;
        return 1;
    }
    
    inline int_n quotient() const {
        return num / dnm;
    }
    
    inline int_n remainder() const {
        return abs_tr<uint_n>( num ) % abs_tr<uint_n>( dnm );
    }
    
    inline void reciprocate() {
        int_n t = num;
        num = dnm;
        dnm = t;
        normalize_sign();
    }
    
    inline Rational reciprocal() const {
        Rational R{ dnm, num };
        R.normalize_sign;
        return R;
    }

#ifdef RATIONAL_OSTREAM_INSERTER
    template<typename U>
    friend std::ostream& operator <<( std::ostream& os, const Rational<Word_T>& c );
#endif
    
private:
    // normalize wide fraction
    static inline void narrow( int_w i_n, int_w i_d, int_n& o_n, int_n& o_d ) {
        if( i_n == 0 ) {
            o_n = 0;
            o_d = 1;
            return;
        }
        
        if( i_d < 0 ) {
            i_n = -i_n;
            i_d = -i_d;
        }
        
        uint_w b = abs_tr<uint_w>( i_n );
        b |= i_d;
        int lz = clz( b );

#ifdef RATIONAL_HI_ACCURACY
        if( lz < BITS_N + 1 ) {
            int shr = BITS_N + 1 - lz;
            int rz = ctz( b );
            if( shr > rz ) {
                gcd<int_w> g = find_gcd( i_n, i_d );
                i_n /= g.n();
                i_d /= g.n();
                
                b = abs_tr<uint_w>( i_n );
                b |= i_d;
                lz = clz( b );
                
#ifdef RATIONAL_EXACT_THROW
                shr = BITS_N + 1 - lz;
                rz = ctz( b );
                if( shr > rz )
                    throw std::out_of_range( "Rational::narrow() : operation results in loss of precision" );
#endif
            }
        }
#endif
        
        if( lz < BITS_N + 1 ) {
            i_n = shr_round( i_n, BITS_N + 1 - lz );
            i_d = shr_round( i_d, BITS_N + 1 - lz );
        }
    
        o_n = static_cast<int_n>( i_n );
        o_d = static_cast<int_n>( i_d );
        assert( o_n != HI_BIT_S );
        assert( o_d != HI_BIT_S );
    }
};

#ifdef RATIONAL_OSTREAM_INSERTER
template<typename Word_T>
std::ostream& operator <<( std::ostream& os, const Rational<Word_T>& r ) {
#ifdef RATIONAL_OSTREAM_PARENS
    os << "(";
#endif
    r.normalize_sign();
    
    os << +r.num;

#ifdef RATIONAL_OSTREAM_DNM1
    os << "/" << +r.dnm;
#else
    if( r.dnm != 1 )
        os << "/" << +r.dnm;
#endif
    
#ifdef RATIONAL_OSTREAM_PARENS
    os << ")";
#endif
    return os;
}
#endif

#endif /* Rational_hpp */
