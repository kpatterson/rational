//
//  Rational_math.hpp
//  Rational
//
//  Created by Kevin H. Patterson on 1/30/21.
//  Copyright © 2021 Creltek. All rights reserved.
//

#ifndef Rational_math_hpp
#define Rational_math_hpp

#include "Rational.hpp"

enum SqrtFlags_e {
    NONE = 0
    , FIX_OVERFLOW = 1
    , FIX_ZERO = 2
    , REDUCE_2 = 4
};

// super fast estimated square root: should give approximately 4 bits of accuracy
template</* SqrtFlags_e */ int Flags = FIX_ZERO, typename T>
Rational<T> sqrt_est( const T& n ) {
    using uint_n = typename std::make_unsigned<T>::type;
    static constexpr int BITS_N = std::numeric_limits<uint_n>::digits;
    static constexpr int BITS_W = BITS_N * (1 + (Flags & FIX_OVERFLOW));
    using uint_w = typename sized_uint<BITS_W>::type;
    
    if( Flags & FIX_ZERO ) {
        if( n == 0 )
            return Rational<T>();
    }
    
    assert( n > 0 );
    
    // a * 2^2p = n
    // sqrt_est( n ) = (0.5a + 0.5) * 2^p
    // This beauty is insanely optimized: 1xCLZ, 2xINC, 1xAND, 1xSUB, 2xADD, 2xSHL, 1xSHR
    const uint_n S = n;
    const unsigned int clz_inc = clz( S ) + 1;
    const unsigned int shl_half = (sizeof(T) << 3) - clz_inc + (clz_inc & 1);
    const uint_w half = T(1) << shl_half;
    const unsigned int shl_dnm = shl_half >> 1;
    uint_w num = S + half;                 // 0.5a + 0.5
    uint_n dnm = T(1) << (shl_dnm + 1);    // normalizer for 2^n
    
    // reduce by powers of 2 (optional)
    if( Flags & REDUCE_2 ) {
        uint_w b = num | dnm;
        const unsigned int tz = ctz( b );
        if( tz ) {
            num >>= tz;
            dnm >>= tz;
        }
    }
    
    // deal gracefully with potential overflow (optional)
    if( Flags & FIX_OVERFLOW ) {
        const int ovfl_shr = (sizeof(num) << 3) - (sizeof(T) << 3) + 1 - clz( num );
        if( ovfl_shr >= 1 ) {
            num >>= ovfl_shr;
            dnm >>= ovfl_shr;
        }
    }
    
    return Rational<T>( num, dnm );
}

// super fast estimated square root: should give approximately 4 bits of accuracy
template</* SqrtFlags_e */ int Flags = FIX_ZERO, typename T>
Rational<T> sqrt_est( const Rational<T>& n ) {
    Rational<T> R = sqrt_est<Flags,T>( n.num ) / sqrt_est<Flags,T>( n.dnm );
    R.reduce();
    return R;
}

#endif /* Rational_math_hpp */
