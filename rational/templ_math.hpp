//
//  templ_math.hpp
//  Rational
//
//  Created by Kevin H. Patterson on 1/30/21.
//  Copyright © 2021 Creltek. All rights reserved.
//

#ifndef templ_math_hpp
#define templ_math_hpp

template<unsigned int N>
struct sized_uint {};
template<> struct sized_uint<8>   { using type=uint8_t; };
template<> struct sized_uint<16>  { using type=uint16_t; };
template<> struct sized_uint<32>  { using type=uint32_t; };
template<> struct sized_uint<64>  { using type=uint64_t; };
#ifdef __SIZEOF_INT128__
template<> struct sized_uint<128> { using type=__uint128_t; };
#endif
#ifdef __SIZEOF_INT256__
template<> struct sized_uint<256> { using type=__uint256_t; };
#endif

// absolute value
template<typename T>
inline T abs( const T& n ) {
    if( n < 0 )
        return -n;
    else
        return n;
}

// absolute value with templated return type
template<typename R, typename T>
inline R abs_tr( const T& n ) {
    if( n < 0 )
        return -n;
    else
        return n;
}

// count number of zero bits from left (msb and down)
template<typename T>
inline unsigned int clz( T n ) {
    assert( n != 0 );
    
    if( std::is_signed<T>::value )
        if( n < 0 ) n = -n;
    
    if( sizeof(T) <= sizeof(unsigned) ) {
        unsigned int b = __builtin_clz( n );
        b -= (sizeof(unsigned) - sizeof(T)) << 3;
        return b;
    }
    
    if( sizeof(T) <= sizeof(unsigned long) ) {
        unsigned int b = __builtin_clzl( n );
        b -= (sizeof(unsigned long) - sizeof(T)) << 3;
        return b;
    }
    
    {
        unsigned int b = __builtin_clzll( n );
        b -= (sizeof(unsigned long long) - sizeof(T)) << 3;
        return b;
    }
}

// count number of zero bits from right (lsb and up)
template<typename T>
inline unsigned int ctz( T n ) {
    assert( n != 0 );
    
    if( std::is_signed<T>::value )
        if( n < 0 ) n = -n;
    
    if( sizeof(T) <= sizeof(unsigned) )
        return __builtin_ctz( n );
    
    if( sizeof(T) <= sizeof(unsigned long) )
        return __builtin_ctzl( n );
    
    return __builtin_ctzll( n );
}

// rounding shift right
template<typename T>
inline T shr_round( const T& n, const unsigned int& sh ) {
    if( std::is_signed< decltype(n) >::value ) {
        if( n < 0 )
            return n >> sh;
        else
            return (-(-n >> sh));
        
    } else {
        using ST = typename std::make_signed<T>::type;
        ST sn = n;
        return (-(-sn >> sh));
    }
}

// gcd = bits * 2^x
template<typename T>
struct gcd {
    T bits;
    unsigned int x;
    
    T n() const { return bits << x; }
};

// implementation of Stein's algorithm
template<typename T>
gcd<T> find_gcd( T u, T v ) {
    if( u == 0 && v == 0 )
        return gcd<T>{ 1, 0 };
    
    if( u == 0 )
        return gcd<T>{ v, 0 };
    
    if( v == 0 )
        return gcd<T>{ u, 0 };
    
    if( std::is_signed<T>::value ) {
        if( u < 0 ) u = -u;
        if( v < 0 ) v = -v;
    }
    
    unsigned int x = ctz( u | v );
    u >>= x;
    v >>= x;
    u >>= ctz( u );
    
    do {
        v >>= ctz( v );
        
        if( u > v ) {
            T t = v; v = u; u = t;
        }
        v -= u;
    } while( v != 0 );
    
    return gcd<T>{ u, x };
}

template<typename T>
inline T find_gcd_n( const T& a, const T& b ) {
    return find_gcd( a, b ).n();
}

#endif /* templ_math_hpp */
